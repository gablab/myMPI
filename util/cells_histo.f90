module modstats
  use modhist
  use modlag  ! ntype, np_local
  implicit none
  class(Histogram),pointer, dimension(:,:) :: hist,histz_ens
  character*40, dimension(:,:) ::  fn,fne
  integer :: itype, ir

contains

  subroutine init_histograms(ifr)
    integer, intent(in) :: ifr
    allocate(histz(ntype,3))
    allocate(fn(ntype,3))
    allocate(histz_ens(ntype,3))
    allocate(fne(ntype,3))
    do ir=1,3
      do itype=1,ntype
        call hist(itype,ir)%hcreate(0.d0,2*PI,50)
        write(fn(itype,ir),"('data/hist_',i1.1,'_',i2.2,'.',i3.3,'.dat')")ir,itype,ifr
        open(97,file=fn(itype,ir),dispose='delete')
        close(97)

        call histz_ens(itype,ir)%hcreate(0.d0,2*PI,50)
        write(fne(itype,ir),"('data/hist_ens_',i1.1,'_',i2.2,'.',i3.3,'.dat')")ir,itype,ifr
        open(97,file=fne(itype,ir),dispose='delete')
        close(97)
      end do
    end do
  end subroutine init_histograms

  subroutine fill_histograms_partial(xp,xptype)
    real*8,dimension (:,:),intent(in) :: xp,xptype
    ! init histogram
    ir=1
    do itype=1,NTYPE
      do ii=1,np_local
        if(xptype(ii).eq.itype) then
          call hist(itype,ir)%hfill(xp(ir,ii))
        end if
      end do
    end do
    !print*,histx%integral(),histy%integral(),histz%integral()
  end subroutine fill_histograms_partial
  
  subroutine fill_histograms_ensemble(xp,xptype)
    real*8,dimension (:,:),intent(in) :: xp,xptype
    ! init histogram
    ir=1
    do itype=1,NTYPE
      do ii=1,np_local
        if(xptype(ii).eq.itype) then
          call hist_ens(itype,ir)%hfill(xp(ir,ii))
        end if
      end do
    end do
    !print*,histx%integral(),histy%integral(),histz%integral()
  end subroutine fill_histograms_ensemble

  subroutine print_histo_partial()
    ir=3
    do itype=1,ntype
      ! print histos
      call hist(itype,ir)%hprint(fn(itype,ir))
      ! new empty histos
      call histz(itype,ir)%hcreate(0.d0,2*PI,50)
    end do
  end subroutine print_histo_partial

  subroutine print_histo_ensemble()
    ir=3
    do itype=1,ntype
      ! print histos
      call hist_ens(itype,ir)%hprint(fne(itype,ir))
    end do
  end subroutine print_histo_ensemble

end module modstats
