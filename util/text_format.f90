      module text_format
      character(len=*), parameter :: Fsmall='(12G12.2)'
      character(len=*), parameter :: Ftinyint='(12G4.0)'
      character(len=*), parameter :: Fprecise='(16G16.6)'
      end module text_format


