module modhist
  use text_format
  implicit none
  real*8, parameter :: PI = 4.D0*DATAN(1.D0)

!  type,public :: Histogram
!    real*8 :: xmin, xmax, binw
!    integer  :: nbin
!    real*8, dimension(:,:), allocatable :: arr !(position,value)
!  contains
!    procedure :: hcreate => histogram_create
!    procedure :: hfill   => histogram_put
!    procedure :: hfill_arr => histogram_put_array
!    procedure :: hfill_file => histogram_fill_file
!    procedure :: hprint  => histogram_print
!    procedure :: normalize => histogram_normalize
!    procedure :: integral => histogram_integral
!  end type Histogram

!  integer :: nsteps,ncurr

!  type,public :: Avg
!    real*8 :: sum
!    integer :: npoints
!  contains 
!    procedure :: init => avg_init
!    procedure :: add => avg_add
!    procedure :: get => avg_get
!  end type Avg


contains



  !  function gauss_unit(x) result(y) ! gaussian adapt to unit domain
  !    real*8, intent(in) :: x
  !    real*8 :: y
  !    real*8 :: sigma, pre, mu
  !    sigma = 1./5
  !    pre = 1./(sigma*sqrt(2*PI))
  !    mu = 1./2
  !    y = pre * exp(-((((x-mu)/sigma)**2))/2)
  !  end function gauss_unit

  !  function uniform_unit(x) result(y) 
  !    real*8, intent(in) :: x
  !    real*8 :: y
  !    y = 1
  !  end function uniform_unit

  ! create histogram
  subroutine histogram_create(arr,xmin,xmax,nbin)
    implicit none
    real*8, allocatable, dimension(:,:), intent(inout) :: arr 
    real*8, intent(in) :: xmin,xmax
    integer, intent(in) :: nbin
    real*8 :: top,btm,binw
    integer :: ii
    btm = min(xmin,xmax)
    top = max(xmin,xmax)
    if(top.eq.btm) then
      top = top + 1
      btm = btm - 1
    end if
    !this%xmax = top
    !this%xmin = btm
    !this%nbin = nbin
    binw = (top-btm)/nbin
    if(allocated(arr)) deallocate(arr)
    allocate(arr(2,nbin))
    do ii=1,nbin
      arr(1,ii) = btm + (ii-1./2)*binw
    end do   
    arr(2,:) =0.
  end subroutine histogram_create

  ! fill histogram with a single datum
  subroutine histogram_put(arr,x)
    implicit none
    real*8, allocatable, dimension(:,:), intent(inout) :: arr 
    real*8, intent(in) :: x
    real*8 :: xmin, xmax, binw
    integer :: ii
    binw = arr(1,2) - arr(1,1)
    xmin = arr(1,1) - binw/2.
    xmax = arr(1,size(arr(2,:))) + binw/2.
    ii = floor((x-xmin)/binw)+1
    if(x.gt.xmax) then
      write(*,*)"histogram: out of range; x = ",x,">",xmax
    else if(x.lt.xmin) then
      write(*,*)"histogram: out of range; x = ",x,"<",xmin
    else
      if(x.eq.xmax) then
        arr(2,size(arr(1,:))) = arr(2,size(arr(1,:))) + 1
      else
        arr(2,ii) = arr(2,ii) + 1
      end if
    end if
  end subroutine histogram_put

  ! fill histogram with an array
  !  subroutine histogram_put_array(this,arr,imax)
  !  class(Histogram),intent(inout) :: this
  !    real*8,dimension(:),intent(in) :: arr
  !    integer,intent(in),optional :: imax
  !    real*8 :: x
  !    integer :: j,jmax
  !    if(present(imax)) then
  !      jmax = imax
  !    else
  !      jmax = size(arr)
  !    end if
  !    do j=1,jmax
  !      x = arr(j)
  !      call this%hfill(x)
  !    end do
  !  end subroutine histogram_put_array


  ! fills histogram reading a file 
  !  (optional: with integer selector)
  !  (optional: select interval)
  !  - avoid lines beginning with #
  !  - provide ids indicating which column is to be considered
  !  - provide number of columns
  !  - selector: provide column of selector and value to be selected
  !  - interval: provide a second value to average 
  !     over different selector values
  !  subroutine histogram_fill_file(this,infile,xcol,ncol,scol,sval,sval2)
  !  class(Histogram),intent(inout) :: this
  !    character(len=*),intent(in) :: infile
  !    integer,intent(in) :: xcol,ncol
  !    integer,intent(in),optional :: scol,sval,sval2 ! selector column and value 
  !    real*8,dimension(:),allocatable :: inputs
  !    integer :: io
  !    character(len=300) :: line,tline
  !    allocate(inputs(ncol))
  !    open(21,file=trim(infile),status="old",action="read")
  !    do 
  !      read(21,'(A)',iostat=io) line !'(A)' reads whole line
  !      if(io.ne.0) exit
  !      tline = trim(line)
  !      call StripSpaces(tline) ! to be improved
  !      if((tline(1:1).eq."#").or.(tline.eq."")) then
  !      else
  !        read(line,*) inputs
  !        if(.not.present(scol)) then ! no scol => no selection
  !          call this%hfill(inputs(xcol))
  !        else ! scol => check if == sval
  !          if(.not.present(sval2)) then ! no sval2 => only a value
  !            if(inputs(scol).eq.sval) then
  !              call this%hfill(inputs(xcol))
  !            end if
  !          else ! sval2 => check if >= sval && <= sval2
  !            if((inputs(scol).ge.sval).and.(inputs(scol).le.sval2)) then
  !             call this%hfill(inputs(xcol))
  !            end if
  !          end if
  !        end if
  !      end if
  !    end do
  !!    close(21)
  !    if(present(sval2)) then
  !      this%arr(:,2) = this%arr(:,2) / dble(sval2-sval)
  !    end if
  !  end subroutine histogram_fill_file

  ! print histogram
  subroutine histogram_print(arr,outfile)
    real*8, allocatable, dimension(:,:), intent(inout) :: arr 
    character(len=*),intent(in) :: outfile
    open(139,file=trim(outfile),action='write',&
      position="append")
    call matrix_print(arr,139)
    close(139)
  end subroutine histogram_print

  ! normalize histogram
  subroutine histogram_normalize(arr,norm)
    real*8, allocatable, dimension(:,:), intent(inout) :: arr 
    real*8 :: norm
    arr(2,:) = arr(2,:)/norm
  end subroutine histogram_normalize

  ! return histogram integral
  function histogram_integral(arr) result(y)
    real*8, allocatable, dimension(:,:), intent(inout) :: arr 
    real*8 :: y
    y = sum(arr(2,:)) 
  end function histogram_integral

  ! Fills histogram; histo is 2D array: (bin position,value)
!  subroutine histogram_fill(x,histo,xmin,xmax,is_ready)
!    real*8, intent(in) :: x, xmin, xmax
!    real*8, intent(inout), dimension(:,:) :: histo
!    integer, intent(inout) :: is_ready
!    integer :: ii, nbin
!    real*8 :: bwidth, xleft, xright
!    nbin = size(histo(:,2))
!    bwidth = (xmax-xmin)*1./nbin
!    if((xmin.le.x).and.(x.le.xmax)) then
!      do ii=1,nbin
!        xleft = xmin+(ii-1)*bwidth
!        xright = xmin + ii*bwidth
!        if(is_ready.eq.0) then
!          histo(ii,1) = (xright+xleft)/2.
!        end if
!        if( (xleft.le.x).and.(xright.gt.x) ) then
!          histo(ii,2) = histo(ii,2) +  1
!        end if
!      end do
!    else
!      print*,"out of range",x
!    end if
!    is_ready = 1
!  end subroutine histogram_fill

!  subroutine histogram_fill_with_array(arr,histo,xmin0,xmax0,masking)
!    real*8, intent(in), dimension(:) :: arr
!    real*8, intent(inout), dimension(:,:) :: histo
!    real*8, optional, intent(in) :: xmin0, xmax0
!    logical,  optional, intent(in), dimension(:) :: masking
!    logical  :: use_mask
!    real*8 :: x, xmin, xmax
!    integer  :: ii, is_ready
!    if(present(masking)) then
!      use_mask = .true.
!    else
!      use_mask = .false.
!    end if
!    if(.not.present(xmin0)) then
!      xmin = MINVAL(arr,mask=masking)
!    else
!      xmin = xmin0
!    end if
!    if(.not.present(xmax0)) then
!      xmax = MAXVAL(arr)
!    !else
!      xmax = xmax0
!    end if
!    histo(:,2) = 0.
!    if(xmin.eq.xmax) then
!      xmin = xmin - 1
!      xmax = xmax + 1
!    end if
!    is_ready = 0
!    do ii=1,size(arr)
!      if(masking(ii).or.(.not.use_mask)) then
!        call histogram_fill(arr(ii),histo,xmin,xmax,is_ready)
!      end if
!    end do  
!  end subroutine histogram_fill_with_array

!  function average(arr) result(y)
!    real*8, intent(in), dimension(:) :: arr
!    real*8 :: y
!    integer :: i, narr
!    narr = size(arr)
!    y = 0
!    do i=1,narr
!      y = y + arr(i)
!    end do
!    y = y/narr
!  end function average

!  function uncertainty(arr) result(y)
!    real*8, intent(in), dimension(:) :: arr
!    real*8 :: y, arr_avg
!    integer :: i, narr
!    narr = size(arr)
!    y = 0 
!    arr_avg = average(arr)
!    do i=1,narr
!      y = y + (arr_avg - arr(i))**2
!    end do
!    y = SQRT(y/narr)
!  end function uncertainty


  ! returns 1 with a probability x and 0 with a p. (1-x)
!  function P(x) result(y)
!    real*8, intent(in) :: x
!    real*8 :: temp
!    logical :: y
!    call RANDOM_NUMBER(temp)
!    if(temp.lt.x) then
!      y = .true.
!    else 
!      y = .false.
!    end if
!  end function P

  ! remove all spaces in the string: 
  ! credits https://stackoverflow.com/a/27179914/5599687
  subroutine StripSpaces(string)
    character(len=*) :: string
    integer :: stringLen 
    integer :: last, actual
    stringLen = len (string)
    last = 1
    actual = 1
    do while (actual .lt. stringLen)
      if (string(last:last) .eq. ' ') then
        actual = actual + 1
        string(last:last) = string(actual:actual)
        string(actual:actual) = ' '
      else
        last = last + 1
        if (actual .lt. last) &
          actual = last
      end if
    end do
  end subroutine



  subroutine matrix_print(u,outfile_int)
    real*8, intent(in) :: u(:,:)
    integer, optional, intent(in) :: outfile_int
    integer :: n
    integer :: M
    M = size(u,2) 
    if( .not.present(outfile_int) ) then
      do n=1,M
        write(*,Fsmall) u(:,n)
      end do
      write(*,Fprecise) ''
    else 
      do n=1,M
        write(outfile_int,Fprecise) u(:,n)
      end do
      write(outfile_int,Fprecise) ''
    end if
  end subroutine matrix_print


!  subroutine avg_init(this)
!  class(Avg),intent(inout) :: this
!    this%sum = 0.d0
!    this%npoints = 0
!  end subroutine avg_init

!  subroutine avg_add(this,x)
!  class(Avg),intent(inout) :: this
!    real*8,intent(in) :: x
!    this%sum = this%sum + x
!    this%npoints = this%npoints + 1
!  end subroutine avg_add

!  function avg_get(this) result(y)
!  class(Avg),intent(inout) :: this
!    real*8 :: y
!    y = this%sum/dble(this%npoints)
!    return 
!  end function avg_get

end module modhist
