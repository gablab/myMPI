program testhist
  use modhist
  use mpi
  implicit none
  !include 'mpif.h'

  ! MPI vars
  integer :: ierr,rank,num_proc
  integer :: tag = 1
  integer :: nrk, prk ! preceding and following processors' ranks 
  integer :: stat(MPI_STATUS_SIZE)

  ! particles vars
  integer :: itype
  integer :: ntype = 5
  real*8, allocatable, dimension(:) :: x
  integer, allocatable, dimension(:) :: xtype
  integer:: npop = 100
  real*8 :: xmax = 3.14159265358979323846

  ! histos vars
  real*8, allocatable, dimension(:,:) :: hist
  integer :: st
  character*40 :: histo_name
  integer :: nbins = 10

  ! dummy
  integer :: ii
  real*8 :: temp

  ! MPI init 
  call MPI_Init(ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD,rank,ierr)
  call MPI_Comm_size(MPI_COMM_WORLD,num_proc,ierr)
  nrk = mod(rank+1,num_proc)
  if(rank.eq.0) then
    prk = num_proc-1
  else
    prk = mod(rank-1,num_proc)
  end if

  ! particles init
  allocate(xtype(npop))
  allocate(x(npop))

  ! generate particles
  do ii=1,npop
    temp = dble(ii**1.3)
    xtype(ii) = mod(int(temp),ntype)+1
    temp = mod(temp,xmax) + rank*xmax
    x(ii) = temp
  end do



  ! histos 
  do itype=1,NTYPE
    call synch_histo(hist,x,xtype)
    ! print histo
    if(rank.eq.0) then
      write(histo_name,"('data/hist_',i2.2,'.dat')")itype
      open(21,file=histo_name,status='old',iostat=st)
      if(st.eq.0) close(21,status='delete')
      call histogram_print(hist,histo_name)
    end if
  end do

  ! partial histos
  do itype=1,NTYPE
    call histogram_create(hist,0.d0,xmax*num_proc,nbins) 
    do ii=1,npop
      if(xtype(ii).eq.itype) then
        call histogram_put(hist,dble(x(ii)))
      end if
    end do 
    write(histo_name,"('data/hist_',i2.2,'_',i2.2,'.dat')")itype,rank
    open(21,file=histo_name,status='old',iostat=st)
    if(st.eq.0) close(21,status='delete')
    call histogram_print(hist,histo_name)
  end do

  call MPI_finalize(ierr)

contains

  subroutine synch_histo(harr,x,xtype)
    use modhist
    real*8, dimension(:,:), allocatable, intent(inout) :: harr
    real*8, dimension(:), intent(in) :: x
    integer, dimension(:), intent(in) :: xtype
    real*8, dimension(:,:), allocatable :: hin, hout
    real*8 :: hmax

    if(allocated(harr)) deallocate(harr)
    if(rank.eq.0) then
      hmax = xmax * num_proc
      call histogram_create(harr,0.d0,hmax,nbins) 
      allocate(hin(size(harr(:,1)),size(harr(1,:))))
      allocate(hout(size(harr(:,1)),size(harr(1,:))))
      ! fill histo
      do ii=1,npop
        if(xtype(ii).eq.itype) then
          call histogram_put(harr,dble(x(ii)))
        end if
      end do 
      hout = harr
      call MPI_Send(hout,2*nbins,MPI_REAL8,nrk,tag,MPI_COMM_WORLD,ierr)
      call MPI_Recv(hin,2*nbins,MPI_REAL8,prk,tag,MPI_COMM_WORLD,stat,ierr)
      harr = hin
    else   
      allocate(hin(2,nbins))
      call MPI_Recv(hin,2*nbins,MPI_REAL8,prk,tag,MPI_COMM_WORLD,stat,ierr)
      allocate(harr(size(hin(:,1)),size(hin(1,:))))
      allocate(hout(size(hin(:,1)),size(hin(1,:))))
      harr = hin
      ! fill histo
      do ii=1,npop
        if(xtype(ii).eq.itype) then
          call histogram_put(harr,dble(x(ii)))
        end if
      end do 
      hout = harr
      call MPI_Send(hout,2*nbins,MPI_REAL8,nrk,tag,MPI_COMM_WORLD,ierr)
    end if
    deallocate(hin)
    deallocate(hout)


  end subroutine


end program
