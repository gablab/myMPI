#! /bin/gnuplot

set terminal qt persist

  binwidth=5
bin(x,width)=width*floor(x/width)
set boxwidth 0.9 relative
set yrange [0:7]
  plot 'data/hist_01.dat' with boxes lc rgb "dark-red" fill pattern 5 title "global", \
    'data/hist_01_00.dat' with boxes fs transparent solid 0.2 lc rgb "dark-blue" lw 0 title "first process", \
    'data/hist_01_01.dat' with boxes fs transparent solid 0.2 lc rgb "forest-green" lw 0 title "second process"

set terminal pngcairo
set output 'plot.png'
replot
