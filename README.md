myMPI
======
self-teaching MPI library (Fortran)
------

+ **histogram** : 

   fill an array shared between more than one process. The plot shows the histogram that each processor would see without sharing, and the complete histogram.

   ![plot of histogram][plot_histo]
   
   
   
   
   
   
   
   
   
   
   
[plot_histo]: https://gitlab.com/gablab/myMPI/raw/master/histogram/plot.png